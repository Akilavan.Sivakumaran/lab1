package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	while (true) {
        	System.out.println("Let's play round " + roundCounter);
        	String humanChoice = userChoice();
        	String cptrChoice = randomChoice();
        	String choiceString = "Human chose " + humanChoice + ", computer chose " + cptrChoice + ".";
        	
        	if (winner(humanChoice, cptrChoice)) {
        		humanScore += 1;
        		System.out.println(choiceString + " Human wins!");
        	}
        	else if (winner(cptrChoice, humanChoice)) {
        		computerScore += 1;
        		System.out.println(choiceString + " Computer wins!");
        	}
        	else {
        		System.out.println(choiceString + " It's a tie!");
        	}
        	System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        	
        	roundCounter += 1;
        	String cont_answer = continue_game();
        	if (cont_answer.equals("n")) {
        		break;
        	}
        }
    	
    	System.out.println("Bye bye :)");
    	
    
    	
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
    //public int randomIndex() {
    	//int rando = (int)(Math.random() * rpsChoices.size());
    	//return rando;
    //}
    
    public String randomChoice() {
    	int rando = (int)(Math.random() * rpsChoices.size());
    	String choice = rpsChoices.get(rando);
    	return choice;
    }

    public boolean validate(String input, List<String> valid_list) {
    	input = input.toLowerCase();
    	return valid_list.contains(input);
    }
    
    public String userChoice() {
    	while (true) {
    		String user = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    		if (validate(user, rpsChoices)) {
    			return user;
    		}
    		else {
    			System.out.println("I don't understand " + user + ". Try again");
    		}
    	}
    }
    
    public String continue_game() {
    	while (true) {
    		String answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
    		if (validate(answer, Arrays.asList("y","n"))) {
    			return answer;
    		}
    		else {
    			System.out.println("I do not understand " + answer + ". Could you try again?");
    		}
    	}
    }
    
    public boolean winner(String p1, String p2) {
    	if (p1.equals("paper")) {
    		//return (p2 == "rock");
    		return (p2.equals("rock"));
    	}
    	else if (p1.equals("rock")) {
    		//return (p2 == "scissors");
    		return (p2.equals("scissors"));
    	} 
    	else {
    		//return (p2 == "paper");
    		return (p2.equals("paper"));
    	}
    }
    
}